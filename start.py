import os
from aiogram.types import message
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
import asyncio
import aioschedule
import logging
from telegram import bot_token

bot = Bot(token=bot_token)
dp = Dispatcher(bot)

# Логи в консоль
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.answer(
        'Начата процедура запуска контейнеров для SMS шлюза на сервере.')
    await start_composes()
    await message.answer(
        'Контейнеры запущены, SMS шлюз работает')


@dp.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    await message.answer(
        'Бот для управления SMS шлюзом. SMS шлюз доступен по адресу 10.30.30.110 \nСписок команд: \n/start - Запустить SMS шлюз \n/help - Справка \n' \
        '/restart - Перезапустить SMS шлюз')


@dp.message_handler(commands=['restart'])
async def process_restart_gammu_command(message: types.Message):
    await message.answer(
        "Перезапуск контейнера gammu1")
    await restart_gammu()
    await message.answer(
        "Контейнер gammu1 перезапущен")


# Добавление правил для создания symlink и рестарт udevadm
def write_rules():
   print('Add rules')
   os.system('cp modem-mts.rules /etc/udev/rules.d/modem-mts.rules')

   print('Restart udevadm')
   os.system('udevadm control --reload-rules && udevadm trigger')


# Старт gammu
def start_gammu():
   os.system('cd gammu; docker-compose up -d')


# Старт playsms
def start_playsms():
   os.system('cd playsms; docker-compose up -d')


# Перезапуск gammu
async def restart_gammu():
   os.system('sh scripts_bash/restart_usb.sh')
   os.system('cd gammu; docker-compose down; docker-compose up -d')


# Проверка есть ли правила для созданим symlink
def check_rules():
   status = os.system('ls /etc/udev/rules.d/modem-mts.rules')

   if status != 0:
       print('No rules')
       return 1
   elif status == 0:
       print('Rules is detected')
       return 0


# Проверка запущен ли gammu
def check_gammu():
   status = os.system('cd gammu; docker-compose ps | grep -i up')

   if status != 0:
       print('Status gammu is not 0')
       return 1
   elif status == 0:
       print('Status gammu is 0')
       return 0


# Проверка запущен ли playsms
def check_playsms():
   status = os.system('cd playsms; docker-compose ps | grep -i up ')
   
   if status != 0:
       print('Status playsms is not 0')
       return 1
   elif status == 0:
       print('Status playsms is 0')
       return 0


# Запуск контейнеров gammu и playsms
async def start_composes():
   chk_rules = check_rules()
   if chk_rules == 1:
       write_rules()
   elif chk_rules == 0:
       pass
      

   chk_play = check_playsms()
   if chk_play == 1:
       print('Starting playsms')
       start_playsms()
   elif chk_play == 0:
       print('Playsms is running')

 
   chk_gammu = check_gammu()
   if chk_gammu == 1:
       print('Starting gammu')
       start_gammu()
   elif chk_gammu == 0:
       print('Gammu is running')

start_composes()


status = {'old': 'empty', 'new': 'empty'}

# Проверка к какому порту подклюсен модем и перезапуск контейнера в случае если порт сменился
def check_ttyusb():
    global status

    usb0 = os.system('ls /dev/ttyUSB0')
    usb1 = os.system('ls /dev/ttyUSB1')

    logs_device = os.system('docker logs gammu1 | grep "Error getting SMS status: Error writing to the device."')
    logs_afk = os.system('docker logs gammu1 | grep "Probably the phone is not connected"')

    if usb0 == 0:
        print('USB is equal to 0')
        status['new'] = 0
    elif usb1 == 0:
        print('USB is equal to 1')
        status['new'] = 1

    if status['old'] == 'empty':
        status['old'] = status['new']
        return print('Old_status was empty')
    elif logs_device == 0 or logs_afk == 0:
        print('Errors in logs')
        restart_gammu()
        return print('Restart gammu')
    elif status['new'] != status['old']:
        status['old'] = status['new']
        for i in range(6):
            print('Not the same')
        restart_gammu()
        return print('Restart gammu')
    if status['new'] == status['old']:
        return print('Same')

# Cron функция
async def run_1():
    check_ttyusb()
    

async def run_2():
    restart_gammu()


async def function_call_1():
    await run_1()


async def function_call_2():
    await run_2()


async def scheduler():
    aioschedule.every(1).minutes.do(function_call_1)
    aioschedule.every(15).minutes.do(function_call_2)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)


async def on_startup(_):
    asyncio.create_task(scheduler())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
