# Подготовка к запуску SMS шлюза

## 1) Создать бота telegram
##### Документация по созданию бота telegram:

[Создать telegram бота (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20telegram%20%D0%B1%D0%BE%D1%82%D0%B0.txt

[Создать telegram бота (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20telegram%20%D0%B1%D0%BE%D1%82%D0%B0.txt

###### [Создать telegram бота (локальный репозиторий)]

###### [Создать telegram бота (удаленный репозиторий)]



## 2) Создать следующие команды для бота telegram

```
start - Запустить SMS шлюз
help - Справка
restart - Перезапустить SMS шлюз
```

##### Документация по созданию команд для бота telegram:

[Создание команд для telegram бота (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%20%D0%B4%D0%BB%D1%8F%20telegram%20%D0%B1%D0%BE%D1%82%D0%B0.txt

[Создание команд для telegram бота (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%20%D0%B4%D0%BB%D1%8F%20telegram%20%D0%B1%D0%BE%D1%82%D0%B0.txt

###### [Создание команд для telegram бота (локальный репозиторий)]

###### [Создание команд для telegram бота (удаленный репозиторий)]



## 3) Установить docker
##### Документация по установке docker:

[Установка Docker (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0%20Docker.txt

[Установка Docker (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/commit/6843bc3734faf28d7de9865353a0b636f861b430

###### [Установка Docker (локальный репозиторий)]

###### [Установка Docker (удаленный репозиторий)]



## 4) Установить docker-compose
##### Документация по установке docker-compose:

[Установка Docker-compose (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0%20Docker.txt

[Установка Docker-compose (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0%20Docker-compose.txt

###### [Установка Docker-compose (локальный репозиторий)]

###### [Установка Docker-compose (удаленный репозиторий)]



## 5) Установить python 3.9.7
##### Документация по установке python:

[Установка Python на Linux (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0%20Python%20%D0%BD%D0%B0%20Linux.txt

[Установка Python на Linux (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0%20Python%20%D0%BD%D0%B0%20Linux.txt

###### [Установка Python на Linux (локальный репозиторий)]

###### [Установка Python на Linux (удаленный репозиторий)]



## 6) Создать виртуальное окружение для проекта check_modem
##### Документация по созданию виртуального окружения python:

[Создание виртального окружения python в linux (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B2%D0%B8%D1%80%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%BE%D0%BA%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F%20python%20%D0%B2%20linux.txt

[Создание виртального окружения python в linux (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B2%D0%B8%D1%80%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%BE%D0%BA%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F%20python%20%D0%B2%20linux.txt

###### [Создание виртального окружения python в linux (локальный репозиторий)]

###### [Создание виртального окружения python в linux (удаленный репозиторий)]



## 7) Установить зависимости


1) Перейти в проект check_modem:

```
cd check_modem/
```

2) Установить зависимости:

```
pip3.9 install -r requirements.txt 
```



# Запуск SMS шлюза


1) Перейти в проект check_modem:

```
cd check_modem/
```

2) Запустить скрипт start.py:

```
python3.9 start.py
```


# Управление SMS шлюзом

##### Дальнейшее управление SMS шлюзом происходит через telegram бота



# Управление и настройка веб-интерфейса playsms

##### Веб-интерфейс доступен по адресу http://10.30.30.110

##### Документация по управлению и настройки веб-интерфейса playsms

[Управление и настройка веб-интерфейса playsms (локальный репозиторий)]:http://10.30.30.223/vadimkubs/wiki/-/blob/main/%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B8%20%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%B2%D0%B5%D0%B1-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81%D0%B0%20playsms.pdf

[Управление и настройка веб-интерфейса playsms (удаленный репозиторий)]:https://gitlab.com/baltgps/wiki/-/blob/main/%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B8%20%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%B2%D0%B5%D0%B1-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81%D0%B0%20playsms.pdf

###### [Управление и настройка веб-интерфейса playsms (локальный репозиторий)]

###### [Управление и настройка веб-интерфейса playsms (удаленный репозиторий)]
